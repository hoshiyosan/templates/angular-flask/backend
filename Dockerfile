FROM python:3.6-slim

WORKDIR /app

# install project dependencies
COPY packages/prod /app/packages
RUN pip install -r packages

# copy project's source code
COPY server.py /app/
COPY src /app/src/

# run backend server in production mode using gunicorn
CMD ["gunicorn", "-b", "0.0.0.0:80", "server:app"]
