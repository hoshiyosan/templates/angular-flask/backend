# ${APP_NAME} backend

## Contribute

### Prerequisites
Before contributing to this project, you need to install the followings tools or softwares.
* make (to work with Makefile commands)
* python3 and pip3 (available on most linux distributions)
* virtualenv package for python3
* docker (optional)

Example for linux
```bash
apt install make python3 python3-pip
pip3 install virtualenv
```

### Install project dependencies

The following command install project's dependencies in a virtualenv (located in `./.env/`). The virtualenv is automatically created if it does not exists.

```bash
make install
```

### Helpful commands

A full list of make commands can be retrieved like so :

```bash
$ make help
install     # install project dependencies in a virtualenv
serve       # run server in debug mode with hot reload enabled
```