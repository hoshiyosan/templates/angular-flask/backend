import os

# select appropriate environment (default to dev)
FLASK_ENV = os.getenv('FLASK_ENV', 'development')

# dev config is always available
if FLASK_ENV == 'development':
    from .dev import *

# prod environment variables overwrite dev's ones
from .prod import *
