from flask import Flask
from .addons import *
from .database import create_db
from .endpoints import create_api
from .. import settings


def create_app():
    app = Flask(__name__)
    app.config.from_object(settings)

    create_db(app)
    create_api(app)

    return app
