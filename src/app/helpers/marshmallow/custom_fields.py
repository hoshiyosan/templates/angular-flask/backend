from marshmallow import fields


class TitleCaseString(fields.String):
    def _serialize(self, value, attr, obj, **kwargs):
        return value

    def _deserialize(self, value, attr, data, **kwargs):
        return None if value is None else value.title()


class UpperCaseString(fields.String):
    def _serialize(self, value, attr, obj, **kwargs):
        return value

    def _deserialize(self, value, attr, data, **kwargs):
        return None if value is None else value.upper()
