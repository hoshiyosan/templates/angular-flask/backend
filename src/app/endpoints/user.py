from flask import request
from flask_restplus import Namespace, Resource
from ..dao import UserDAO

ns = Namespace('Users', 'Manage users and their friends', path='/users')


@ns.route('')
class UserResource(Resource):
    def get(self):
        """
        Browse users
        """
        instance = UserDAO.browse()
        return UserDAO.dump(instance)

    def post(self):
        """
        Create user
        """
        instance = UserDAO.create(request.json)
        return UserDAO.dump(instance)


@ns.route('/<int:user_id>')
class UserResource(Resource):
    def get(self, user_id):
        """
        Get user's details
        """
        instance = UserDAO.get_or_404(id=user_id)
        return UserDAO.dump(instance)
