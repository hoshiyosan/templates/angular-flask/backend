from flask_restplus import Namespace, Resource

ns = Namespace('Authentication', 'Token-based authentication endpoints', path='/auth')

@ns.route('/token')
class TokenResource(Resource):
    def get(self):
        """
        Retrieve a new access_token from a refresh token.
        """
        return {
            "access_token": "regtrg54tr8h4tr65h4tr"
        }
    
    def post(self):
        """
        Retrieve a new access_token from credentials.
        """
        return {
            "access_token": "regtrg54tr8h4tr65h4tr"
        }
        
    def delete(self):
        """
        Revoke access_token used to perform this query.
        """
        return "revoked"