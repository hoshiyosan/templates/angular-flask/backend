import marshmallow
import sqlalchemy

from typing import Dict
from flask import abort

from ..addons import db, ma


class BaseDAO:
    model: db.Model
    schemas: Dict[str, ma.ModelSchema]
    
    @classmethod
    def name(cls):
        return cls.model.__tablename__
    
    @classmethod
    def get_or_404(cls, **kwargs):
        instance = cls.model.query.filter_by(**kwargs).first()
        
        if instance is None:
            abort(404, "%s not found."%cls.name())
        
        return instance
    
    @classmethod
    def browse(cls, **kwargs):
        instances = cls.model.query.filter_by(**kwargs).all()
        return instances
        
    @classmethod
    def create(cls, data: dict, schema='default'):
        if data is None:
            return abort(400, "Error creating %s. Request body is empty."%cls.name())
        
        # use modelschema to load object instance from dict
        schema = cls.schemas[schema]()
        
        try:
            instance = schema.load(data)
        except marshmallow.exceptions.ValidationError as e:
            # raised when input validation failed
            return abort(400, e.args)
        
        # add object to database
        try:
            db.session.add(instance)
            db.session.commit()
        except sqlalchemy.exc.IntegrityError:
            return abort(409, "Unable to create %s. .. details .."%cls.name())
        
        # return instance to confirm object creation
        return instance
    
    @classmethod
    def dump(cls, instance_or_list, schema='default'):
        many = False if isinstance(instance_or_list, cls.model) else True
        schema = cls.schemas[schema](many=many)
        return schema.dump(instance_or_list)
