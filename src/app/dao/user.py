from .base import BaseDAO
from ..database.models import User
from ..database.schemas import UserSchema


class UserDAO(BaseDAO):
    model = User
    schemas = {
        'default': UserSchema
    }
